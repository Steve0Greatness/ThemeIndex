<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
    version="3.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:t="themes.xsd"
>

<xsl:output method="html" version="5.0" encoding="UTF-8" indent="yes"/>

<xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">
        <head>
            <title>Roundabout Theme Index</title>
            <link rel="stylesheet" href="style.css" />
            <meta charset="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <meta http-equiv="X-UA-Compatible" content="ie=edge" />
            <link rel="icon" href="favicon.ico" />
        </head>
        <body>
            <hgroup role="group">
                <h1>Roundabout Theme Index</h1>
                <p class="subtitle">The hub for Roundabout Themes</p>
            </hgroup>
            <details class="resources">
                <summary>Help and Resources</summary>
                <ul>
                    <li>Don't know how to install a theme? See <a href="install.html">install.html</a></li>
                    <li>Want your own theme here? See <a href="submit.html">submit.html</a></li>
                    <li>Source code available at <a href="https://roundabout-host.com/steve0greatness/ThemeIndex">roundabout-host.com/steve0greatness/ThemeIndex</a></li>
                    <li>Join our Matrix: <a href="https://matrix.to/#/#roundaboutthemeindex:matrix.org">#roundaboutthemeindex:matrix.org</a></li>
                </ul>
                <strong>Theme Creation</strong>
                <p>This section is for resources about theme creation.</p>
                <ul>
                    <li>Roundabout default theme: <a href="https://roundabout-host.com/roundabout/roundabout/tree/master/static/efficient-ui/THEME.css">roundabout-host.com/roundabout/roundabout/tree/master/static/efficient-ui/THEME.css</a></li>
                    <li>Roundabout stylesheet: <a href="https://roundabout-host.com/roundabout/roundabout/tree/master/static/style.css">roundabout-host.com/roundabout/roundabout/tree/master/static/style.css</a></li>
                </ul>
            </details>
            <hr />
            <h2 id="theme-list">Theme List</h2>
            <ul id="ThemeList">
                <xsl:for-each select="/t:themes/t:theme">
                    <li>
                        <table>
                            <tr>
                                <th
                                    class="theme-name" colspan="2"
                                >
                                    <xsl:value-of select="t:title"/>
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="t:author/@uri"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="t:author"/>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <a>
                                        <xsl:attribute name="href">
                                            themes/<xsl:value-of select="t:uri"/>
                                        </xsl:attribute>
                                        Theme CSS
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </li>
                </xsl:for-each>
            </ul>
        </body>
    </html>
</xsl:template>

</xsl:stylesheet>
